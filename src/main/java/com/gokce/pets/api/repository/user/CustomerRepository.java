package com.gokce.pets.api.repository.user;


import com.gokce.pets.api.domain.Customer;

public interface CustomerRepository extends BaseUserRepository<Customer> {
}
