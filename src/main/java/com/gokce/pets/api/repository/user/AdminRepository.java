package com.gokce.pets.api.repository.user;


import com.gokce.pets.api.domain.Admin;

public interface AdminRepository extends BaseUserRepository<Admin> {
}
