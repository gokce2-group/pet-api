package com.gokce.pets.api.service;

import com.gokce.pets.api.domain.Customer;
import com.gokce.pets.api.domain.Device;
import com.gokce.pets.api.util.LocationUpdateRequest;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.FirebaseDatabase;
import lombok.extern.log4j.Log4j2;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;

@Service
@Log4j2
public class FirebaseService {
    private static final String           JSON = "{\n" +
            "  \"type\": \"service_account\",\n" +
            "  \"project_id\": \"gsmproject-f1f09\",\n" +
            "  \"private_key_id\": \"696805263aaf01ff467c312e98da22fe3495503b\",\n" +
            "  \"private_key\": \"-----BEGIN PRIVATE KEY-----\\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDZyOw8OtL2L3xY\\nOl1kNbtjyRp6VT/M3OO9c4MgOUY8he/Gs5M0gfONi6JDenLobuu/oxtN55qh3Hrv\\n4KUDafkbNVe5+vt8qauVPKtdWMrWpYvNBc675Z/rR70AJDYLT7BZm0DhOCjjn0+X\\nEw5bp9W/5bTzUUGnvy4S+JYZtqkSsWIagFjSozowmk5NG2HGBBruN57gFQ4A/4bJ\\nu6JShqiptWnfzYwwSFmUgjwGe9ObWPYt8NMyeybdAxstzwDCBYKBuv5b/mPTAmEr\\nQNw8ZoHbScXGidxlsgU0b1n3m6rXRhz8VISSAGzeLjvS3YiIpy2SmnVGvnm1Qwoc\\nZAZji/GXAgMBAAECggEAFfvduOC5PeeiNifCGKUB4vtAZHvpgk0D4jJgWKbfa6mb\\ngCPNZr39b1raiNQ5+DOGMaSN04nfBprCgCddkOAoKLJ0Rg5LR5S7AMgMhlI5u3wf\\nhhUhfr8Zqv7lWbfOCstDAnCVFob8Pantt1LRnrfs8D+ISg14Qq4eQQT95d+V5pB4\\nmVLaxagmhpOUfg6TubcGW9mSmQa2ZC4KpRCRqxzvCZhzEILXkH55Tn4hbdkuWmWk\\nZ6jEkQJazgv35nmCSHtpysVkpsKUFS6d5D5msSUrTeENZ4PzHeITmGkKHLd2T123\\nh3LGNyR9CnDoG2YXwAtl6rKxIGHPNVEaCxplOBfjUQKBgQD5CflgiDPulHwks/CQ\\n6vQVIvE8TtP834Qg76GvZdWRU/eF9kYA7uJkIdQdxGVEKF4okS/UsgeMfQdE0eIx\\nphJIRGJ2d/PpH9jxiuyXSlP/GiDRqx8cs9/Jv23AZ+Rp23N6l7oNoVpTtmWl33b5\\njp2JSYd3+L45X7kJ8uXLTNHdkQKBgQDf305y/5356WRCaslE5Y8jsTwJHY63NABK\\nRrcvZqQzAI0CgPRWcL4ZUkYai0d8IuV9IVer6WGsle+mW4+Y6hTYNocp6MDJdR9z\\nCXoZHpRIeZhtuqkpdcGJC4hIrLxXak07Ae8C3yFrI0INw6QK0jZ0URl/UuEYwvZR\\npmjMr8nopwKBgFRGnc+fCAP617AL/KLt/7+PXZeNDYwLj8kdjteuXIGbqq3402PJ\\nc29zSzbvmV1hU8vipg8SWB+gx+7wH92xV4qBKdjxauc/UckbvEjObHJtwyFxCUxL\\nSrYZQ+jSv6dbDk80PVAThF/D384sOzTkoEcvTNq6oJUF4baOLlmIG5UhAoGACiYa\\nbVYvODQfam9i3m+1tH2FZjhjzuRZmwRnq4twqCs/OTzVVtHXnFhBe1STEgqW8rw/\\nODjlR6ioa5c9BEagWUjnibvWeYuIuHNWiKr9N+fjRB4xKwL+mhLew8So6SGVjLZC\\nupOY9T1s8ijLV/49n7pJeJ8uOywjdIIhwB27/oMCgYAXcs8fjX5vB0m7Gqbxs5JV\\nT96Z+RI2cS/Hbv8Bn04/BTiPwALIlQA5+E2goQW8A4psfkYotR36KJh9QzPw+GEl\\n4mHi1ZmVDwWVmOc/xEi08EMVS2N5B9dCQijI63p74nTnKystlImZ0oiBQz2oKQwZ\\n9akBk/SpxF2+pv7jzbVbuQ==\\n-----END PRIVATE KEY-----\\n\",\n" +
            "  \"client_email\": \"firebase-adminsdk-ujpfr@gsmproject-f1f09.iam.gserviceaccount.com\",\n" +
            "  \"client_id\": \"114409269809127666103\",\n" +
            "  \"auth_uri\": \"https://accounts.google.com/o/oauth2/auth\",\n" +
            "  \"token_uri\": \"https://oauth2.googleapis.com/token\",\n" +
            "  \"auth_provider_x509_cert_url\": \"https://www.googleapis.com/oauth2/v1/certs\",\n" +
            "  \"client_x509_cert_url\": \"https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-ujpfr%40gsmproject-f1f09.iam.gserviceaccount.com\"\n" +
            "}";
    private final        FirebaseDatabase database;

    public FirebaseService() {
        FirebaseApp.initializeApp(getOptions());
        database = FirebaseDatabase.getInstance();
    }

    private FirebaseOptions getOptions() {
        try {
            return new FirebaseOptions.Builder()
                    .setDatabaseUrl("https://gsmproject-f1f09.firebaseio.com/")
                    .setCredentials(GoogleCredentials.fromStream(new ByteArrayInputStream(JSON.getBytes())))
                    .build();
        } catch (java.io.IOException e) {
            e.printStackTrace();
            System.exit(0);
            return null;
        }
    }

    public void save(@NonNull Customer customer, @NonNull Device device, @NonNull LocationUpdateRequest data) {
        database.getReference("data").child(String.valueOf(customer.getId()))
                .child(String.valueOf(device.getId()))
                .setValue(data, (error, ref) -> {
                    if (error != null) {
                        log.error("Failed to save data to firebase because {}", error.getMessage());
                    } else {
                        log.info("Data for device {} saved successfully", device.getId());
                    }
                });
    }

}
