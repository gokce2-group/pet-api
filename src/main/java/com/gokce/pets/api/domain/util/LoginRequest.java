package com.gokce.pets.api.domain.util;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;

@Getter
@AllArgsConstructor
public class LoginRequest implements Serializable {
    private final String username;
    private final String password;
    private final String deviceId;
}
