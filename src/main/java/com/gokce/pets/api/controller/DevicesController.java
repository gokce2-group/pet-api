package com.gokce.pets.api.controller;

import com.gokce.pets.api.domain.Admin;
import com.gokce.pets.api.domain.Device;
import com.gokce.pets.api.repository.DeviceRepository;
import com.gokce.pets.api.service.DeviceService;
import com.gokce.pets.api.util.DeviceRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/devices")
@CrossOrigin("*")
public class DevicesController {
    private static final ResponseEntity<?> unauthorized = ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    private final        DeviceRepository  deviceRepository;
    private final        DeviceService     deviceService;

    public DevicesController(DeviceRepository deviceRepository, DeviceService deviceService) {
        this.deviceRepository = deviceRepository;
        this.deviceService    = deviceService;
    }


    @PostMapping("/save")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> update(@RequestBody DeviceRequest request,
                                    @AuthenticationPrincipal Admin admin) {
        if (admin == null) {
            return unauthorized;
        }
        Device device = deviceService.save(request);
        return device != null ? ResponseEntity.ok(device) : ResponseEntity.badRequest().build();
    }

    @GetMapping("/get")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> get(@AuthenticationPrincipal Admin admin) {
        if (admin == null) {
            return unauthorized;
        }
        return ResponseEntity.ok(deviceRepository.findAll());
    }

}
