package com.gokce.pets.api.controller.user;


import com.gokce.pets.api.domain.Admin;
import com.gokce.pets.api.domain.Authority;
import com.gokce.pets.api.domain.util.LoginRequest;
import com.gokce.pets.api.repository.user.AdminRepository;
import com.gokce.pets.api.repository.util.LoginLogRepository;
import com.gokce.pets.api.security.JwtTokenUtil;
import com.gokce.pets.api.service.LoginService;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/admins")
@CrossOrigin("*")
public class AdminController {
    private final AdminRepository                      adminRepository;
    private final LoginService<Admin, AdminRepository> loginService;

    public AdminController(AdminRepository adminRepository,
                           LoginLogRepository loginLogRepository,
                           JwtTokenUtil tokenUtil) {
        this.adminRepository = adminRepository;
        this.loginService    = new LoginService<>(adminRepository, loginLogRepository, tokenUtil);
        if (adminRepository.findByUsername("admin@takip.com").isEmpty()) {
            insertAdmin();
        }
    }

    private void insertAdmin() {
        Admin admin = new Admin();
        admin.setAuthority(Authority.ROLE_ADMIN);
        admin.setEnabled(true);
        admin.setId(1L);
        admin.setName("Hayvan Takip Admin");
        admin.setPassword(BCrypt.hashpw("papatya", BCrypt.gensalt()));
        admin.setUsername("admin@takip.com");
        adminRepository.save(admin);
    }


    @PostMapping("/login")
    public ResponseEntity<?> login(@Validated @RequestBody LoginRequest loginRequest,
                                   @NonNull HttpServletRequest request) {
        return loginService.login(loginRequest, request);
    }

}
