package com.gokce.pets.api.controller.user;


import com.gokce.pets.api.domain.Admin;
import com.gokce.pets.api.domain.Authority;
import com.gokce.pets.api.domain.Customer;
import com.gokce.pets.api.domain.util.LoginRequest;
import com.gokce.pets.api.repository.user.CustomerRepository;
import com.gokce.pets.api.repository.util.LoginLogRepository;
import com.gokce.pets.api.security.JwtTokenUtil;
import com.gokce.pets.api.service.DeviceService;
import com.gokce.pets.api.service.LoginService;
import com.gokce.pets.api.util.CustomerRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

@RestController
@CrossOrigin("*")
@RequestMapping("/customers")
public class CustomerController {
    private final        CustomerRepository                         baseRepository;
    protected final      JwtTokenUtil                               tokenUtil;
    private final        LoginService<Customer, CustomerRepository> loginService;
    private static final ResponseEntity<?>                          unauthorized = ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    private final        DeviceService                              deviceService;

    public CustomerController(CustomerRepository baseRepository,
                              LoginLogRepository loginLogRepository,
                              JwtTokenUtil tokenUtil,
                              DeviceService deviceService) {
        this.tokenUtil      = tokenUtil;
        this.baseRepository = baseRepository;
        this.deviceService  = deviceService;
        loginService        = new LoginService<>(baseRepository, loginLogRepository, tokenUtil);
        if (baseRepository.findByUsername("papatya").isEmpty()) {
            Customer customer = new Customer();
            customer.setUsername("papatya");
            customer.setAuthority(Authority.ROLE_CUSTOMER);
            customer.setCreationTime(System.currentTimeMillis());
            customer.setEnabled(true);
            customer.setName("Sleeping Papatya");
            customer.setPassword(BCrypt.hashpw("okbb", BCrypt.gensalt()));
            baseRepository.save(customer);
        }
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/save")
    public ResponseEntity<?> insert(@Validated @RequestBody CustomerRequest request,
                                    @AuthenticationPrincipal Admin admin) {
        Customer customer = null;
        if (request.getId() > 0) {
            customer = baseRepository.findById(request.getId()).orElse(null);
        }
        if (customer == null) {
            customer = new Customer();
            customer.setUsername(request.getUsername());
            customer.setCreationTime(System.currentTimeMillis());
        }
        Objects.requireNonNull(admin);
        customer.setAuthority(getAuthority());
        if (request.getPassword() != null && request.getPassword().length() > 3) {
            customer.setPassword(BCrypt.hashpw(request.getPassword(), BCrypt.gensalt()));
        }
        customer.setName(request.getName());
        customer.setEnabled(request.isEnabled());
        customer = baseRepository.save(customer);
        return ResponseEntity.ok(customer);
    }

    @NonNull
    public Authority getAuthority() {
        return Authority.ROLE_CUSTOMER;
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@Validated @RequestBody LoginRequest loginRequest,
                                   HttpServletRequest request) {
        return loginService.login(loginRequest, request);
    }

    @PostMapping("/updatePushToken")
    public ResponseEntity<?> updateToken(@RequestParam String token,
                                         @AuthenticationPrincipal Customer user) {
        return loginService.updateToken(token, user);
    }

    @GetMapping("/devices")
    public ResponseEntity<?> userDevices(@AuthenticationPrincipal Customer customer) {
        if (customer == null) {
            return unauthorized;
        }
        return ResponseEntity.ok(deviceService.devices(customer));
    }

    @GetMapping("/get")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> getCustomers(@AuthenticationPrincipal Admin admin) {
        if (admin == null) {
            return unauthorized;
        }
        return ResponseEntity.ok(baseRepository.findAll());
    }

}
