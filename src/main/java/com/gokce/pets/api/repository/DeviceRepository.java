package com.gokce.pets.api.repository;

import com.gokce.pets.api.domain.Customer;
import com.gokce.pets.api.domain.Device;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;

import java.util.List;

public interface DeviceRepository extends JpaRepository<Device,Long> {
    List<Device> findAllByUser(@NonNull Customer customer);
}
