package com.gokce.pets.api.repository;

import com.gokce.pets.api.domain.Device;
import com.gokce.pets.api.domain.util.DbNotification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;

import java.util.List;

public interface NotificationRepository extends JpaRepository<DbNotification,Long> {
    List<DbNotification> findAllByDeviceOrderByTimeDesc(@NonNull Device device);
}
