package com.gokce.pets.api.util;

import com.fasterxml.jackson.annotation.JsonProperty;

public record JunkResponse(@JsonProperty int code, @JsonProperty String message) {

    public static JunkResponse success() {
        return new JunkResponse(200, "success");
    }
}
