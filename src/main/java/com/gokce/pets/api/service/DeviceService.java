package com.gokce.pets.api.service;

import com.gokce.pets.api.domain.Customer;
import com.gokce.pets.api.domain.Device;
import com.gokce.pets.api.domain.util.LatLng;
import com.gokce.pets.api.repository.DeviceRepository;
import com.gokce.pets.api.repository.user.CustomerRepository;
import com.gokce.pets.api.util.DeviceRequest;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DeviceService {
    private final CustomerRepository customerRepository;
    private final DeviceRepository   deviceRepository;

    public DeviceService(CustomerRepository customerRepository,
                         DeviceRepository deviceRepository) {
        this.customerRepository = customerRepository;
        this.deviceRepository   = deviceRepository;
        if (deviceRepository.count() < 1) {
            insertDummy();
        }
    }

    private void insertDummy() {
        customerRepository.findByUsername("papatya")
                .ifPresent(customer -> {
                    save(new DeviceRequest("Dummy Cow", 0, 0), customer);
                });
    }

    public Device save(@NonNull DeviceRequest request) {
        if (request.getUser() > 0) {
            Optional<Customer> optionalCustomer = customerRepository.findById(request.getUser());
            return optionalCustomer.map(customer -> save(request, customer)).orElse(null);
        } else {
            return save(request, null);
        }
    }

    public Device save(@NonNull DeviceRequest request, @Nullable Customer customer) {
        if (request.getDevice() > 0) {
            return deviceRepository.findById(request.getDevice())
                    .map(device -> {
                        device.setUser(customer);
                        device.setName(request.getName());
                        deviceRepository.save(device);
                        return device;
                    }).orElse(null);
        }
        Device device = Device.builder()
                .name(request.getName())
                .user(customer)
                .location(LatLng.builder()
                        .lng(35.240741)
                        .lat(38.9573415)
                        .build())
                .build();
        return deviceRepository.save(device);
    }

    public List<Device> devices(@NonNull Customer customer) {
        return deviceRepository.findAllByUser(customer);
    }
}
