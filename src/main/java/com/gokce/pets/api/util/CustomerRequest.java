package com.gokce.pets.api.util;

import lombok.*;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class CustomerRequest implements Serializable {
    private String  name;
    private long    id;
    private boolean enabled;
    private String  password;
    private String  username;
}
