package com.gokce.pets.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Getter
@Setter
@MappedSuperclass
public abstract class User implements UserDetails {
    @Column(nullable = false)
    private String    name;
    @Column(nullable = true)
    private String    password;
    @Column(nullable = false)
    private boolean   enabled;
    @Column(unique = true, nullable = false)
    private String    username;
    @Column(nullable = false)
    private Authority authority;
    @Column(name = "device_id")
    private String    deviceId;
    @Column(name = "push_token")
    private String    pushToken;
    @Column(name = "create_time")
    private Long      creationTime;
    @Column(name = "expire_date")
    private Long      expireDate;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Transient
    @JsonInclude
    private String accessToken;

    @Override
    @Transient
    @JsonIgnore
    public Collection<SimpleGrantedAuthority> getAuthorities() {
        return List.of(authority.toGranterAuthority());
    }

    public boolean isEnabled() {
        if (expireDate != null) {
            return System.currentTimeMillis() < expireDate;
        }
        return enabled;
    }

    @Override
    @Transient
    public boolean isAccountNonExpired() {
        return enabled;
    }

    @Override
    @Transient
    public boolean isAccountNonLocked() {
        return enabled;
    }

    @Override
    @Transient
    public boolean isCredentialsNonExpired() {
        return enabled;
    }
}
