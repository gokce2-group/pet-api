package com.gokce.pets.api.util;

import lombok.*;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class DeviceRequest implements Serializable {
    private String name;
    private long   user;
    private long   device;
}