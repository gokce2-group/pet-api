package com.gokce.pets.api.util;

import com.gokce.pets.api.domain.Device;
import lombok.*;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class LocationUpdateRequest implements Serializable {
    private long          device;
    private double        lat;
    private double        lng;
    private Device.Status status;

}
