package com.gokce.pets.api.repository.util;


import com.gokce.pets.api.domain.util.LoginLog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LoginLogRepository extends JpaRepository<LoginLog, Long> {
}
