package com.gokce.pets.api.repository.user;


import com.gokce.pets.api.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.lang.NonNull;

import java.util.Optional;

@NoRepositoryBean
public interface BaseUserRepository<U extends User> extends JpaRepository<U, Long> {
    Optional<U> findByUsername(@NonNull String username);
}
