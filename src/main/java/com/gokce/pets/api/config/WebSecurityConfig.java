package com.gokce.pets.api.config;


import com.gokce.pets.api.security.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.lang.NonNull;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final JwtUserDetailsServiceImpl   userDetailsService;
    private final JwtAuthenticationEntryPoint unauthorizedHandler;
    private final TokenKeyUtil                tokenKeyUtil;


    public WebSecurityConfig(JwtUserDetailsServiceImpl userDetailsService,
                             JwtAuthenticationEntryPoint unauthorizedHandler,
                             TokenKeyUtil tokenKeyUtil) {
        this.userDetailsService  = userDetailsService;
        this.unauthorizedHandler = unauthorizedHandler;
        this.tokenKeyUtil        = tokenKeyUtil;
    }

    @Autowired
    void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }


    @Bean
    public JwtTokenUtil tokenUtil(@NonNull TokenKeyUtil util) {
        return new JwtTokenUtil(util);
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .cors().and()
                .exceptionHandling().authenticationEntryPoint(unauthorizedHandler)
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers(Endpoints.getUnauthorized()).permitAll()
                .anyRequest()
                .authenticated();

        JwtAuthenticationTokenFilter authenticationTokenFilter = new
                JwtAuthenticationTokenFilter(userDetailsService, tokenUtil(tokenKeyUtil));
        http.addFilterBefore(authenticationTokenFilter, UsernamePasswordAuthenticationFilter.class);

        // disable page caching
        http.headers()
                .frameOptions()
                .sameOrigin()
                .cacheControl();
    }
}