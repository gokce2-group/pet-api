package com.gokce.pets.api.domain.util;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "login_logs")
public class LoginLog implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long   id;
    @Column(nullable = false, name = "user_id")
    private Long   userId;
    @Column(nullable = false, name = "time")
    private long   time;
    @Column(nullable = false, name = "ip")
    private String ip;
    @Column(nullable = false, name = "auth_result")
    private Result result;

    public enum Result {
        FAILED, SUCCESS
    }

}
