package com.gokce.pets.api.service;


import com.gokce.pets.api.domain.User;
import com.gokce.pets.api.domain.util.LoginLog;
import com.gokce.pets.api.domain.util.LoginRequest;
import com.gokce.pets.api.repository.user.BaseUserRepository;
import com.gokce.pets.api.repository.util.LoginLogRepository;
import com.gokce.pets.api.security.JwtTokenUtil;
import com.gokce.pets.api.util.JunkResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.security.crypto.bcrypt.BCrypt;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@Log4j2
public class LoginService<U extends User, R extends BaseUserRepository<U>> {
    private final R                  repository;
    private final LoginLogRepository logRepository;
    private final JwtTokenUtil       tokenUtil;

    public LoginService(@NonNull R repository,
                        @NonNull LoginLogRepository logRepository,
                        @NonNull JwtTokenUtil tokenUtil) {
        this.repository    = repository;
        this.logRepository = logRepository;
        this.tokenUtil     = tokenUtil;
    }

    public ResponseEntity<?> login(@NonNull LoginRequest loginRequest,
                                   @NonNull HttpServletRequest request) {
        Optional<U> adminOptional = repository.findByUsername(loginRequest.getUsername());
        if (adminOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        U admin = adminOptional.get();
        if (!admin.isEnabled()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body("Account expired");
        }
        if (BCrypt.checkpw(loginRequest.getPassword(), admin.getPassword())) {
            log(admin, request, LoginLog.Result.SUCCESS);

            admin.setDeviceId(loginRequest.getDeviceId());
            repository.save(admin);
            admin.setAccessToken(tokenUtil.generateToken(admin.getUsername(),
                    admin.getAuthority(), loginRequest.getDeviceId()));
            admin.setPassword(null);
            return ResponseEntity.ok(admin);
        }
        log(admin,request, LoginLog.Result.FAILED);
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                .body("Wrong password");
    }


    private void log(@NonNull U admin,
                     @NonNull HttpServletRequest request,
                     @NonNull LoginLog.Result result) {
        logRepository.save(LoginLog.builder()
                .userId(admin.getId())
                .result(result)
                .ip(request.getRemoteAddr())
                .time(System.currentTimeMillis())
                .build());
    }

    @NonNull
    public ResponseEntity<?> updateToken(@NonNull String token,
                                         @NonNull U user) {
        user.setPushToken(token);
        repository.save(user);
        return ResponseEntity.ok(JunkResponse.success());
    }

    public ResponseEntity<?> resetDevice(@NonNull Long id) {
        repository.findById(id)
                .ifPresent(user -> {
                    user.setDeviceId(null);
                    repository.save(user);
                });
        return ResponseEntity.ok(JunkResponse.success());
    }
}
