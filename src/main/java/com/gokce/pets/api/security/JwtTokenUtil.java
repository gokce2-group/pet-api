package com.gokce.pets.api.security;

import com.gokce.pets.api.domain.Authority;
import com.gokce.pets.api.domain.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Component
@SuppressWarnings({"unused", "WeakerAccess"})
public class JwtTokenUtil implements Serializable {

    private static final long serialVersionUID = -3301605591108950415L;

    @Value("${jwt.expiration}")
    private       Long         expiration;
    private final TokenKeyUtil keyUtil;

    public JwtTokenUtil(TokenKeyUtil keyUtil) {
        this.keyUtil = keyUtil;
    }

    public String getUsernameFromToken(String token) {
        return getClaimFromToken(token, Claims::getSubject);
    }

    public String generateToken(String username,
                                @NonNull Authority authority,
                                @Nullable String deviceId) {
        return generateTokenSpecificDate(username, authority, deviceId, new Date());
    }

    public String generateTokenSpecificDate(String username,
                                            @NonNull Authority authority,
                                            @Nullable String deviceId,
                                            @NonNull Date date) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("authority", authority);
        if (deviceId != null) {
            claims.put("deviceId", deviceId);
        }
        return doGenerateToken(claims, username, date);
    }


    public Date getIssuedAtDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getIssuedAt);
    }

    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parserBuilder()
                .setSigningKey(keyUtil.parsingKey())
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    public Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    private Boolean isCreatedBeforeLastPasswordReset(Date created, long lastPasswordReset) {
        Date date = new Date(lastPasswordReset);
        return created.before(date);
    }


    private String doGenerateToken(Map<String, Object> claims, String subject,
                                   @NonNull Date date) {
        final Date expirationDate = calculateExpirationDate(date);
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(date)
                .setExpiration(expirationDate)
                .signWith(keyUtil.signingKey())
                .compact();
    }

    public Boolean canTokenBeRefreshed(String token, long lastPasswordReset) {
        final Date created = getIssuedAtDateFromToken(token);
        return !isCreatedBeforeLastPasswordReset(created, lastPasswordReset)
                && (!isTokenExpired(token));
    }

    public String refreshToken(String token) {
        final Date   createdDate    = new Date();
        final Date   expirationDate = calculateExpirationDate(createdDate);
        final Claims claims         = getAllClaimsFromToken(token);
        claims.setIssuedAt(createdDate);
        claims.setExpiration(expirationDate);

        return Jwts.builder()
                .setClaims(claims)
                .signWith(keyUtil.signingKey())
                .compact();
    }

    public Authority getAuthorityFromToken(@NonNull String token) {
        Claims claims = getAllClaimsFromToken(token);
        return Authority.valueOf(claims.get("authority", String.class));
    }

    @Nullable
    public String getDeviceIdFromToken(@NonNull String token) {
        Claims claims = getAllClaimsFromToken(token);
        if (claims.containsKey("deviceId")) {
            return claims.get("deviceId").toString();
        }
        return null;
    }

    public boolean validateToken(String token, UserDetails userDetails) {
        User         user     = (User) userDetails;
        final String username = getUsernameFromToken(token);
        final Date   created    = getIssuedAtDateFromToken(token);
        final Date   expiration = getExpirationDateFromToken(token);
        return (
                username.equals(user.getUsername())
                        && !isTokenExpired(token)
        );
    }

    private Date calculateExpirationDate(Date createdDate) {
        return new Date(createdDate.getTime() + expiration * 1000);
    }
}
