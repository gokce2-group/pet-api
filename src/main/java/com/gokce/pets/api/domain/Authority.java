package com.gokce.pets.api.domain;

import lombok.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.io.Serializable;

public enum Authority implements Serializable {
    ROLE_CUSTOMER,ROLE_ADMIN;
    public SimpleGrantedAuthority toGranterAuthority() {
        return new SimpleGrantedAuthority(this.name());
    }
    @Nullable
    public static Authority createFromValue(@NonNull String value) {
        return switch (value) {
            case "ROLE_CUSTOMER" -> ROLE_CUSTOMER;
            case "ROLE_ADMIN" -> ROLE_ADMIN;
            default -> null;
        };
    }
}
