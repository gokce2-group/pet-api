package com.gokce.pets.api.domain;


import java.io.Serializable;

import com.gokce.pets.api.domain.util.LatLng;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "devices")
public class Device implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long     id;
    @Basic
    @Column(nullable = false)
    private String   name;
    @ManyToOne
    private Customer user;
    @Embedded
    private LatLng   location;
    private Status   status;

    public enum Status {
        ACTIVE, STOPPED, PAUSED
    }

    public boolean isOffline() {
        return status == Status.STOPPED;
    }
}
