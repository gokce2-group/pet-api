package com.gokce.pets.api.security;

import com.google.common.collect.Lists;
import org.springframework.lang.NonNull;

import java.util.ArrayList;
import java.util.List;

public final class Endpoints {
    private static final List<String> unauthorized = new ArrayList<>();
    private static final List<String> swagger      = Lists.newArrayList(
            "/v2/api-docs", "/configuration/ui",
            "/swagger-resources/**", "/configuration/**",
            "/swagger-ui.html", "/webjars/**");

    static {
        unauthorized.add("/ws/**");
        unauthorized.add("/img/**");
        unauthorized.add("/admins/login");
        unauthorized.add("/customers/login");
        unauthorized.add("/app/devices");
        unauthorized.add("/app/send/**");
        unauthorized.add("/app/save/**");
        unauthorized.add("/app/save");
        unauthorized.addAll(swagger);
    }

    @NonNull
    public static String[] getUnauthorized() {
        return unauthorized.toArray(new String[0]);
    }
}
