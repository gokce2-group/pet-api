package com.gokce.pets.api.service;

import com.gokce.pets.api.domain.Device;
import com.gokce.pets.api.domain.util.DbNotification;
import com.gokce.pets.api.repository.NotificationRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

@Service
public class NotificationService {
    private final NotificationRepository notificationRepository;

    public NotificationService(NotificationRepository notificationRepository) {
        this.notificationRepository = notificationRepository;
    }

    public void save(@NonNull Device device) {
        notificationRepository.save(DbNotification.builder()
                .device(device)
                .location(device.getLocation())
                .status(device.getStatus())
                .time(System.currentTimeMillis())
                .build());
    }
}
