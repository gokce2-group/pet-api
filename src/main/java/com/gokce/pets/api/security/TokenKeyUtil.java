package com.gokce.pets.api.security;


import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

@Component
@Log4j2
public class TokenKeyUtil {
    private final KeyPair key;

    public TokenKeyUtil() throws Exception {
        byte[] savedPub = null;
        byte[] savedSec = null;
        try {
            savedPub = IOUtils.resourceToByteArray("keys/pub.key", getClass().getClassLoader());
            savedSec = IOUtils.resourceToByteArray("keys/sec.key", getClass().getClassLoader());
        } catch (IOException e) {
            log.error("Saved keys cannot be read because {}", e.getLocalizedMessage());
        }
        if (savedPub != null && savedSec != null) {
            key = fromStrings(savedPub, savedSec);
            log.info("Read key from resources files");
        } else {
            log.info("Generating key pair...");
            key = KeyPairGenerator.getInstance("RSA").generateKeyPair();
            exportKeyPair();
        }
    }

    private void exportKeyPair() throws IOException {
        log.info("Exporting key pair...");
        PrivateKey privateKey = key.getPrivate();
        PublicKey  publicKey  = key.getPublic();

        // Store Public Key.
        X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(
                publicKey.getEncoded());
        File             file = new File("pub.key");
        FileOutputStream fos  = new FileOutputStream(file);
        fos.write(x509EncodedKeySpec.getEncoded());
        fos.close();
        log.info("Exported pub key to {}", file.getAbsolutePath());

        // Store Private Key.
        PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(
                privateKey.getEncoded());
        file = new File("sec.key");
        fos  = new FileOutputStream(file);
        fos.write(pkcs8EncodedKeySpec.getEncoded());
        fos.close();
        log.info("Exported sec key to {}", file.getAbsolutePath());
    }

    private KeyPair fromStrings(byte[] pub, byte[] sec) throws NoSuchAlgorithmException, InvalidKeySpecException {
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PKCS8EncodedKeySpec secEnc =
                new PKCS8EncodedKeySpec(sec);
        X509EncodedKeySpec pubEnc =
                new X509EncodedKeySpec(pub);
        PrivateKey privateKey = keyFactory.generatePrivate(secEnc);
        PublicKey  publicKey  = keyFactory.generatePublic(pubEnc);
        return new KeyPair(publicKey, privateKey);
    }


    public Key parsingKey() {
        return key.getPublic();
    }

    public Key signingKey() {
        return key.getPrivate();
    }
}
