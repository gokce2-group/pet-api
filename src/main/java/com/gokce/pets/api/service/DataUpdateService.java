package com.gokce.pets.api.service;

import com.gokce.pets.api.domain.util.LatLng;
import com.gokce.pets.api.repository.DeviceRepository;
import com.gokce.pets.api.util.DataToLocationConverter;
import com.gokce.pets.api.util.LocationUpdateRequest;
import lombok.extern.log4j.Log4j2;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class DataUpdateService {
    private final DeviceRepository deviceRepository;
    private final DataToLocationConverter converter;
    private final FirebaseService firebaseService;
    private final NotificationService notificationService;

    public DataUpdateService(DeviceRepository deviceRepository,
                             DataToLocationConverter converter,
                             FirebaseService firebaseService,
                             NotificationService notificationService) {
        this.deviceRepository = deviceRepository;
        this.converter = converter;
        this.firebaseService = firebaseService;
        this.notificationService = notificationService;
    }

    public void save(@NonNull LocationUpdateRequest request) {
        log.info("Saving data for device {}", request.getDevice());
        deviceRepository.findById(request.getDevice())
                .ifPresentOrElse(device -> {
                    device.setLocation(LatLng.builder()
                            .lat(request.getLat())
                            .lng(request.getLng())
                            .build());
                    device.setStatus(request.getStatus());
                    deviceRepository.save(device);
                    if (device.getUser() == null) {
                        log.error("Device has no customer assigned. Cannot save data. Device id is {}", device.getId());
                    }
                    notificationService.save(device);
                    firebaseService.save(device.getUser(), device, request);
                }, () -> {
                    log.error("Device not found for id {}", request.getDevice());
                });
    }
}
