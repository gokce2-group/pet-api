package com.gokce.pets.api.security;


import com.gokce.pets.api.domain.Authority;
import com.gokce.pets.api.domain.User;
import com.gokce.pets.api.repository.user.AdminRepository;
import com.gokce.pets.api.repository.user.CustomerRepository;
import org.springframework.lang.NonNull;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class JwtUserDetailsServiceImpl implements UserDetailsService {
    private final AdminRepository    adminRepository;
    private final CustomerRepository customerRepository;

    public JwtUserDetailsServiceImpl(AdminRepository adminRepository,
                                     CustomerRepository customerRepository) {
        this.adminRepository    = adminRepository;
        this.customerRepository = customerRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<? extends User> user = customerRepository.findByUsername(username);
        if (user.isPresent()) {
            return user.get();
        }
        user = adminRepository.findByUsername(username);
        if (user.isEmpty()) {
            throw new UsernameNotFoundException(String.format("User with username %s not found.", username));
        }
        return user.get();
    }

    public User loadUserByUsername(String username,
                                   @NonNull Authority authority) throws UsernameNotFoundException {
        Optional<? extends User> user = switch (authority) {
            case ROLE_ADMIN -> adminRepository.findByUsername(username);
            case ROLE_CUSTOMER -> customerRepository.findByUsername(username);
        };

        if (user.isEmpty()) {
            throw new UsernameNotFoundException(String.format("User with username %s not found.", username));
        }
        return user.get();
    }
}
