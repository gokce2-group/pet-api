package com.gokce.pets.api.domain.util;

import com.gokce.pets.api.domain.Device;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Table(name = "device_notifications")
public class DbNotification implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long          id;
    @ManyToOne(optional = false)
    private Device        device;

    @Column(nullable = false)
    private Device.Status status;
    @Column(nullable = false)
    @Embedded
    private LatLng        location;
    @Column(nullable = false)
    private Long          time;

}
