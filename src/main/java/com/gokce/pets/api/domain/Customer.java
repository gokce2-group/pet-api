package com.gokce.pets.api.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name = "customers")
@Entity
@Getter
@Setter
public class Customer extends User {


}
