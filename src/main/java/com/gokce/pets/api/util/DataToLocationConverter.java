package com.gokce.pets.api.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gokce.pets.api.domain.util.LatLng;
import lombok.extern.log4j.Log4j2;
import org.json.JSONObject;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class DataToLocationConverter {
    private final ObjectMapper objectMapper;

    public DataToLocationConverter() {
        objectMapper = new ObjectMapper();
    }

    private static boolean isValidJson(@NonNull String data) {
        try {
            new JSONObject(data);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Data format #1# where 1 is device id #<deviceId>#
     *
     * @param data string to parse
     * @return -1 if id not found otherwise device id
     */
    public long findDeviceId(@Nullable String data) {
        if (data == null || data.length() < 2) {
            return -1;
        }
        if (!data.startsWith("#")) {
            return -1;
        }
        data = data.substring(1); //delete leading #
        int hashIndex = data.indexOf('#');
        if (hashIndex == -1 || hashIndex == 0) {
            return -1;
        }
        String possibleId = data.substring(0, hashIndex);
        try {
            return Long.parseLong(possibleId);
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    /**
     * Data format GGA,141408.000,,,,,0,00,4.1,,,,,,*5B
     *
     * @param data request string
     * @return nullable converted request
     */
    @Nullable
    public LatLng convert(@NonNull String data) {
        try {
            data = data.replace("GGA,", "");
            String[] parts = data.split(",");
            if (parts.length < 4) {
                return null;
            }
            String lat = parts[1];
            String lng = parts[3];
            double realLat = toComputerValue(lat);
            double realLng = toComputerValue(lng);
            if (realLat == -1 || realLng == -1) {
                return null;
            }
            return LatLng.builder().lat(realLat).lng(realLng).build();
        } catch (Exception e) {
            return null;
        }
    }

    //converting 4124.8963 format to coordinate in double
    private double toComputerValue(@Nullable String degreeValue) {
        if (degreeValue == null || degreeValue.length() < 3) {
            return -1;
        }
        if (degreeValue.startsWith("0")) {
            degreeValue = degreeValue.substring(1);
        }
        String degree = degreeValue.substring(0, 2);
        String minutes = degreeValue.substring(2);
        try {
            double d = Double.parseDouble(degree);
            double m = Double.parseDouble(minutes);
            return d + (m / 60);
        } catch (NumberFormatException e) {

            return -1;
        }
    }

}
