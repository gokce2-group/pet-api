package com.gokce.pets.api;

import com.gokce.pets.api.domain.util.LatLng;
import com.gokce.pets.api.util.DataToLocationConverter;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ApiApplicationTests {
    @Autowired
    private DataToLocationConverter converter;

    @Test
    void contextLoads() {
    }

    @Test
    public void parseSuccess() {
        String data = "GGA,141408.000,4100.3377,N,02850.9542,E,0,00,4.1,,,,,,*5B";
        LatLng location = converter.convert(data);
        assert location != null;
    }

    @Test
    public void deviceIdSuccess() {
        String data = "#2#GGA,141408.000,4100.3377,N,02850.9542,E,0,00,4.1,,,,,,*5B";
        long deviceId = converter.findDeviceId(data);
        assert deviceId == 2;
    }
}
