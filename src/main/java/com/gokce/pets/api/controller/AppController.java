package com.gokce.pets.api.controller;

import com.gokce.pets.api.domain.Device;
import com.gokce.pets.api.domain.util.LatLng;
import com.gokce.pets.api.service.DataUpdateService;
import com.gokce.pets.api.util.DataToLocationConverter;
import com.gokce.pets.api.util.JunkResponse;
import com.gokce.pets.api.util.LocationUpdateRequest;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/app")
@Log4j2
public class AppController {
    private final DataUpdateService updateService;
    private final DataToLocationConverter converter;

    public AppController(DataUpdateService updateService,
                         DataToLocationConverter converter) {
        this.updateService = updateService;
        this.converter = converter;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST, consumes = "text/plain")
    public ResponseEntity<?> newWay(@RequestBody String data) {
        data = data.replace("\0", "").replaceAll("[^\\x20-\\x7E]", "").trim();
        long deviceId = converter.findDeviceId(data);
        int startIndex = data.indexOf("GGA");
        if (startIndex > -1) {
            data = data.substring(startIndex);
            int endIndex = data.indexOf("$");
            if (endIndex > -1) {
                data = data.substring(0, endIndex);
            } else {
                data = data.substring(startIndex);
            }
        }
        log.error("Data is {}", data);

        LatLng location = converter.convert(data);
        if (location == null || deviceId == -1) {
            log.error("Device did not send location :(");
            return ResponseEntity.ok(JunkResponse.success());
        }
        updateService.save(LocationUpdateRequest.builder()
                .device(deviceId)
                .status(Device.Status.ACTIVE)
                .lat(location.getLat())
                .lng(location.getLng())
                .build());
        return ResponseEntity.ok(JunkResponse.success());
    }

}
